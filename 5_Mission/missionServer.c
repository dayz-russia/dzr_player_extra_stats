modded class MissionServer
{
    ref array<string> m_NullArray = new array<string>;
	string m_NullString = "";
    string m_StringConfig;
    int m_IntConfig;
    string m_StringFirstLineFromArray;
	ref map<PlayerBase, int> m_PlayerOnlineLog;
	string m_StoreFolder = "$profile:/DZR/players/local_player_db"
	int m_IncrementInterval = 10000;
	//ref array<PlayerBase, int> m_PlayerOnlineLog = new array<PlayerBase, int>;
	
	void MissionServer()
	{
		Print("[dzr_player_extra_stats] ::: Starting Serverside");
		ref array<string> m_CfgArray = new array<string>;
		//m_StringConfig = dzr_player_extra_stats_CFG.GetFile("$profile:\\DZR\\dzr_player_extra_stats", "StringConfig.txt", m_NullArray, "MyString");
		//m_IntConfig = dzr_player_extra_stats_CFG.GetFile("$profile:\\DZR\\dzr_player_extra_stats", "IntConfig.txt", m_NullArray, "1").ToInt();
		//m_StringFirstLineFromArray = dzr_player_extra_stats_CFG.GetFile("$profile:\\DZR\\dzr_player_extra_stats", "ArrayConfig.txt", m_CfgArray, "Line1\r\nLine2");
		
		m_PlayerOnlineLog = new map<PlayerBase, int>;
	}
	
	
	override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
	{
		super.InvokeOnConnect(player, identity);
		
		
		if (player)
		{
			string SteamID = player.GetIdentity().GetPlainId();
			string m_PlayerName = player.GetIdentity().GetName();
			
			if(!FileExist( m_StoreFolder+"/"+SteamID+"/"+"TimePlayed.txt") )
			{
				dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "TimePlayed.txt", m_NullArray, "0");
			}
			if(!FileExist( m_StoreFolder+"/"+SteamID+"/"+"DaysPlayed.txt") )
			{
				dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "DaysPlayed.txt", m_NullArray, "0");
			}
			if(!FileExist( m_StoreFolder+"/"+SteamID+"/"+"HoursPlayed.txt") )
			{
				dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "HoursPlayed.txt", m_NullArray, "0");
			}
			if(!FileExist( m_StoreFolder+"/"+SteamID+"/"+"WeeksPlayed.txt") )
			{
				dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "WeeksPlayed.txt", m_NullArray, "0");
			}
			if(!FileExist( m_StoreFolder+"/"+SteamID+"/"+"MonthsPlayed.txt") )
			{
				dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "MonthsPlayed.txt", m_NullArray, "0");
			}
			if(!FileExist( m_StoreFolder+"/"+SteamID+"/"+"YearsPlayed.txt") )
			{
				dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "YearsPlayed.txt", m_NullArray, "0");
			}
			
			
			Print("First connect. Created time files for player "+m_PlayerName+" ("+SteamID+")");
			
			if(m_PlayerOnlineLog.Contains( player ))
			{
				m_PlayerOnlineLog.Set( player, 0 );
			}
			else
			{
				m_PlayerOnlineLog.Insert( player, 0 );
			}
			StartOnlineTimer(player);
		}
	}
	
	override void InvokeOnDisconnect(PlayerBase player)
	{
		StopOnlineTimer(player);
		
		super.InvokeOnDisconnect(player);
		
		
	}
	
	void StartOnlineTimer(PlayerBase player)
	{
		//Print("Timer started");
		GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.AddPlayerMinute, m_IncrementInterval, false, player);
	}
	
	void AddPlayerMinute(PlayerBase player)
	{
		if( m_PlayerOnlineLog.Contains( player ) )
		{
			int increment = m_PlayerOnlineLog.Get( player );
			if( player.IsAlive() )
			{
				//Print("Incremented timer");
				increment = m_PlayerOnlineLog.Get( player ) + 1;
			}
			string SteamID = player.GetIdentity().GetPlainId();
			int m_IntOnlineTime_Stored = dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "TimePlayed.txt", m_NullArray).ToInt();
			int CurrentTime = m_IntOnlineTime_Stored + increment;
			CheckTime(CurrentTime, player);
			
			
			
			m_PlayerOnlineLog.Set( player, increment );
			//Print(m_PlayerOnlineLog.Get( player ));
			GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.AddPlayerMinute, m_IncrementInterval, false, player);
		}
	}
	
	
	void StopOnlineTimer(PlayerBase player)
	{
		//save online time here
		//Print("Saved player online time.");
		string SteamID = player.GetIdentity().GetPlainId();
		string m_PlayerName = player.GetIdentity().GetName();
		int m_IntOnlineTime_Stored = dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "TimePlayed.txt", m_NullArray).ToInt();
		int NewTime = m_IntOnlineTime_Stored + m_PlayerOnlineLog.Get( player );
		
		m_IntOnlineTime_Stored = dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "TimePlayed.txt", m_NullArray, NewTime.ToString(), IO_Command.GET, "Both", 0 ).ToInt();
		
		Print("Player "+m_PlayerName+" played " + m_PlayerOnlineLog.Get( player ) + " minutes. "+m_IntOnlineTime_Stored+" minutes total.");
		//Print("Total New Online: " +m_IntOnlineTime_Stored + " minutes");
		
		//reset player timer
		
		m_PlayerOnlineLog.Remove( player );
		//Print( m_PlayerOnlineLog.Get( player ) );
	}	
	
	void CheckTime(int TimePlayed, PlayerBase player)
	{
		//check total time
		switch(TimePlayed)
		{
			case 3:
			SendAchievement("Achievement Unlocked: Still Alive?", "Spend 3 minutes on the server.", player);
			RewardOnConnect("3minStarter", player);
		}
		//hours played
		int PlayedHours = dzr_player_extra_stats_CFG.GetFile(m_StoreFolder+"/"+SteamID, "TimePlayed.txt", m_NullArray).ToInt();
		switch(PlayedHours)
		{
			case 1:
			SendAchievement("Achievement Unlocked: Lime It Here?", "Spend 1 hour on the server.", player);
			GiveItemsOnConnect("1hourPack", player);
		}
		//days played
		//weeks played
		//months played
		//years played
	}
	
	void SendAchievement(string warningTitle, string warningText, PlayerBase player)
	{
		NotificationSystem.SendNotificationToPlayerIdentityExtended( player.GetIdentity() , 12, warningTitle, warningText, "set:ccgui_enforce image:MapShieldBooster");
	}
	
	void GiveItemsOnConnect(string ItemSet, PlayerBase player)
	{
		SendAchievement("Your Rewards: "+ItemSet, "Your reward will be at your feet next time you connect.", player);
	}
	
}