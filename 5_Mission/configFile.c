

class dzr_player_extra_stats_CFG {
    enum IO_Command
	{
		GET			= 0,
		DELETE			= 1,
		APPEND			= 2,
		GET_ARRAY			= 3,
	}
	enum IO_Flag
	{
		GET			= 0,
	}

	ref array<string> m_NullArray = new array<string>;
	string m_NullString = "";
	
	static string GetFile(string folderPath, string fileName, out ref array<string> contentsArray, string newContents = "", int f_IOCommand = IO_Command.GET, string execSide = "Both", bool NoChangeIfExists = 1)
	{
		
		
		bool isDiagServer 		 = GetGame().IsMultiplayer() && GetGame().IsDedicatedServer();   
		bool isDiagMPClient		 = GetGame().IsMultiplayer() && !GetGame().IsDedicatedServer();  
		
		
		string m_TxtFileName = fileName;
		string fileContent;
		FileHandle fhandle;
		string defaultContents;
		string pth = folderPath +"/"+ m_TxtFileName;
		
		if ( FileExist(folderPath +"/"+ m_TxtFileName) && (f_IOCommand == IO_Command.GET || f_IOCommand == IO_Command.APPEND || IO_Command.GET_ARRAY) )
		{
			
			
			
			fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);		
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			
			fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0)
			{
			
				contentsArray.Insert(line_content);
			}
			
			
			CloseFile(fhandle);
			
			if(!NoChangeIfExists)
			{
				if(newContents != "")
				{
					if(newContents != fileContent)
					{
						fileContent = newContents;
						
						if( f_IOCommand == IO_Command.APPEND)
						{
							fhandle = OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.APPEND);
						}
						else 
						{
							fhandle = OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.WRITE);
						};
						
						FPrintln(fhandle, fileContent);
						CloseFile(fhandle);
					};
					
					Print("[dzr_player_extra_stats] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" Updated to: "+fileContent);
				}
			}
			return fileContent;
		}
		else 
		{
			
			TStringArray parts();
			string path = folderPath;
			path.Split("/", parts);
			path = "";
			foreach (string part: parts)
			{
				path += part + "/";
				if (part.IndexOf(":") == part.Length() - 1)
				continue;
				
				if(f_IOCommand == IO_Command.GET)
				{
					if (!FileExist(path) && !MakeDirectory(path))
					{
						Print("Could not make dirs from path: " + path);
						return "Could not make dirs from path: " + path;
					}
				}
				
				if(f_IOCommand == IO_Command.DELETE)
				{
					DeleteFile(pth);
					Print("Deleted file: "+pth);
					return "Deleted file: "+pth;
				}
			}
			
			
			FileHandle file = OpenFile(pth, FileMode.WRITE);
			if(newContents == "")
			{
				newContents = "You tried to create a file using dzr_player_extra_stats, but did not provide file contents. So this default content is added.";
			}
			if (file != 0 && f_IOCommand == IO_Command.GET)
			{
				FPrintln(file, newContents);
				CloseFile(file);
				
				
				
				fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);		
				FGets( fhandle,  fileContent );
				CloseFile(fhandle);
				
				fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);
				string line_content2;
				while ( FGets( fhandle,  line_content2 ) > 0)
				{
				
					contentsArray.Insert(line_content2);
				}
				
				
				Print("[dzr_player_extra_stats] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" is OK! Contents: "+fileContent);
				CloseFile(fhandle);
				
				return fileContent;
			}
		}
		return "[config] isDiagServer:"+isDiagServer+" isDiagMPClient:"+isDiagMPClient+" :::  File "+folderPath +"/"+ m_TxtFileName+" ERROR!";
	}
}
