class CfgPatches
{
	class dzr_player_extra_stats
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_player_extra_stats
	{
		type = "mod";
		author = "DayZ Russia";
		description = "Collects various player stats and stores on the server. Can be used to rewards, unlocks, achievements etc. Will track online time total on the server, daily, monthly connects,, etc.";
		dir = "dzr_player_extra_stats";
		name = "dzr_player_extra_stats";
		//inputs = "dzr_player_extra_stats/Data/Inputs.xml";
		dependencies[] = {"Core", "Game", "World", "Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_player_extra_stats/Common",  "dzr_player_extra_stats/1_Core"};
			};
			class gameLibScriptModule
			{
				files[] = {"dzr_player_extra_stats/Common",  "dzr_player_extra_stats/2_GameLib"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_player_extra_stats/Common",  "dzr_player_extra_stats/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_player_extra_stats/Common", "dzr_player_extra_stats/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_player_extra_stats/Common", "dzr_player_extra_stats/5_Mission"};
			};

		};
	};
};